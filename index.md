# The title

* main topic

# Table of contents
::: incremental

* item1
* item2
* item3
:::

# Objectives

<blockquote>
clever quote
</blockquote>

:::notes
:::

# What next?

# Made with
* ![revealJS](https://static.slid.es/reveal/logo-v1/reveal-white-text.svg)
* [pandoc](https://pandoc.org/index.html)

# Questions

# Thanks

